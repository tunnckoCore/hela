/* eslint-disable import/no-extraneous-dependencies, node/no-unpublished-import */
import process from 'process';
import test from 'mukla';
import { hela, shell } from '../src';

test('foo bar', () => {
  const cli = hela();
  test.ok(typeof cli === 'object');

  cli.action('foo', () => {
    // not called...
  });

  // shell('echo "bay $PATH"', {
  //   env: {
  //     CI_AUTO_RELEASE_TAG: 1234,
  //   },
  // });

  return shell('git --version').then(() => {
    const commitTitle = process.env.CI_COMMIT_TITLE;

    if (!commitTitle.includes('skip ci')) {
      return shell([
        'git config user.email "$GITLAB_USER_EMAIL"',
        'git config user.name "$GITLAB_USER_NAME"',
        'git tag -a v0.2.0 -m "chore(release): 0.2.0 - trying CI release [skip ci]"',
        'git push origin --tags',
      ]);
    }

    return true;
  });
});
